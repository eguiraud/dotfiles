#!/bin/bash

zenity --question --text="execute backup?"
SHOULDBACKUP=$?
if [[ $SHOULDBACKUP -ne 0 ]]; then
   echo "user selected NO backup"
   exit
fi

echo "asking user for borg archive password..."
export BORG_PASSPHRASE=$(zenity --password)

set -ex

TOBACKUP=(Fun Nextcloud Sync .password-store .ssh .task)
pushd /home/blue
notify-send "backing up $(echo ${TOBACKUP[@]})..." # the extra `echo` is to work around word splitting, which confuses notify-send
borg create --stats '/run/media/blue/backups/home_backup::{now}' "${TOBACKUP[@]}"
notify-send "backup finished"
popd

