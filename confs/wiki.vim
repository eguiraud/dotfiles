"Wiki mode

"Standard initialization
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc

"Wiki mode tweaks
set wrap
set spell spelllang=en

command GrepTag execute 'silent grep! "^\#"<cword>' | copen | redraw!

"Add command to list all existing tags
function WikiTags()
   set splitright
   vnew
   "vertical resize 40
   setlocal buftype=nowrite
   setlocal bufhidden=delete
   setlocal noswapfile
   setlocal nobuflisted
   setlocal nospell
   execute '0read!rg --no-filename -o -N "^\#\\w+" | sort | uniq'
   normal Gddgg
   nnoremap <buffer> <CR> :GrepTag<CR>
endfunction
nnoremap <C-T> :call WikiTags()<CR>

"List backlinks
function WikiBackLinks()
   let f = @%
   let f = "'" . f[:-4] . "]]'"
   exec 'silent grep! ' . f
endfunction
nnoremap <C-B> :call WikiBackLinks()<CR>

"Jump to internal link using CTRL-]
nnoremap <C-]> Bt]vi]"zy:e <C-R>z.md<CR>

"Usually <leader>f maps to :GGrep, but the wiki is not a git repo
nnoremap <leader>f :Grep 
