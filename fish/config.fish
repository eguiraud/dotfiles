# Fix gpg signing of git commits with pinentry-tty
set -x GPG_TTY (tty)

# For ssh-agent to work
set -x SSH_AUTH_SOCK "$XDG_RUNTIME_DIR/ssh-agent.socket"

# Add autojump
source /usr/share/autojump/autojump.fish

# Point ripgrep to its config file
set -x RIPGREP_CONFIG_PATH "/home/blue/.config/ripgrep/rg.conf"

# fzf configuration
set -x FZF_DEFAULT_COMMAND "fd . \$dir --type f"
set -x FZF_DEFAULT_OPTS "--bind ctrl-a:select-all,ctrl-d:deselect-all,ctrl-t:toggle-all"
set -x FZF_CTRL_T_COMMAND "$FZF_DEFAULT_COMMAND"

# opt out of .NET telemetry
set -x DOTNET_CLI_TELEMETRY_OPTOUT 1

# for Valgrind to work properly (https://bbs.archlinux.org/viewtopic.php?id=276422)
set -x DEBUGINFOD_URLS https://debuginfod.archlinux.org

# for Android Studio on Wayland (https://stackoverflow.com/questions/33424736/intellij-idea-14-on-arch-linux-opening-to-grey-screen/34419927#34419927)
set -x _JAVA_AWT_WM_NONREPARENTING 1

if status --is-interactive
   fzf_key_bindings # set fzf key bindings
end

# autocompletion for my own commands
complete -c wiki -f -a "(fd --extension md --base-directory /home/blue/Sync/brain | string trim -l -c '.' | string trim -l -c '/')"
