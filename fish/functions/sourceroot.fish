function sourceroot
    set nargs (count $argv)
    if [ $nargs -ne 1 ]
        echo "One argument required" >&2
        return 1
    end
    set rootdir ~/ROOT/$argv[1]
    set builddir {$rootdir}/cmake-build
    set thisroot {$builddir}/install/bin/thisroot.fish
    echo "sourcing $thisroot"
    source {$thisroot}
end
