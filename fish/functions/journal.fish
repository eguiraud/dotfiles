function journal
   set -l FNAME "/home/blue/Sync/brain/journal"/(date +'%G')".md.gpg"

   if test ! -e $FNAME
      echo -e "# "(date +'My journal for %G')"\n\n#journal" | gpg -r enrico -e - > $FNAME
   end

   gpg -d $FNAME | vipe --suffix md | gpg -r enrico -e - | sponge $FNAME
end
