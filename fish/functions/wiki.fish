# Defined in /tmp/fish.jQxouz/wiki.fish @ line 2
function wiki --wraps='helix'
  argparse 'r/root-dir=' -- $argv
  or return
  if set -q '_flag_r'
     set notes_dir $_flag_r
  else
     set notes_dir "/home/blue/Sync/brain"
  end
  helix -w $notes_dir $notes_dir/$argv
end
