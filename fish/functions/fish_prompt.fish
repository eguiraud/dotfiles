function fish_prompt
   set -x __fish_git_prompt_showdirtystate 'yes'
   set -x __fish_git_prompt_showstashstate 'yes'
   set -x __fish_git_prompt_showupstream 'yes'
   set -x __fish_git_prompt_color_branch yellow
   set -x __fish_git_prompt_char_dirtystate '*'
   set -x __fish_git_prompt_char_stagedstate '^'
   set -x __fish_git_prompt_char_stashstate '@'
   set -x __fish_git_prompt_char_upstream_ahead '↑'
   set -x __fish_git_prompt_char_upstream_behind '↓'

   set last_status $status
   set_color $fish_color_cwd
   printf '%s' (prompt_pwd)
   set_color normal
   printf '%s ' (__fish_git_prompt)
   set_color normal
end
