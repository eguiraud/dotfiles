# Defined in /tmp/fish.d1np6a/work_journal.fish @ line 2
function work_journal
   set -l fname (date +'%g_%m')"_work.md"
   if test ! -e ~/Sync/brain/$fname
      echo -e "# "(date +'%B %Y')" work journal\n\n#workjournal" > ~/Sync/brain/$fname
   end
   wiki $fname
end
