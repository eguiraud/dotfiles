# Defined in /tmp/fish.EyVxD2/make_flamegraph.fish @ line 2
function make_flamegraph --description 'Create a flamegraph' --argument name options
   set fname (string replace --all ' ' '_' {$name}).svg
   perf script  | stackcollapse-perf.pl | flamegraph.pl --title "$name" $options > {$fname}
end
