# usage: b [ncores] [build-dir]
function b
   argparse -X2 -- $argv
   if test (count $argv) -gt 0
      set ncores $argv[1]
   else
      set ncores (nproc)
   end

   if test (count $argv) -gt 1
      set builddir $argv[2]
   else if test -e './CMakeCache.txt'
      set builddir '.'
   # else we check if a cmake-build* or build* directory is present in $PWD
   # cmake-build takes precedence because ROOT sources have a `build` directory
   else
      set possible_builddirs cmake-build* build*
      if test (count $possible_builddirs) -gt 0
         set builddir $possible_builddirs[1]
      else
         echo 'Could not find a build directory' >&2
         return 1
      end
   end

   set cmd cmake --build $builddir -- -j $ncores install
   echo $cmd
   eval $cmd
end
