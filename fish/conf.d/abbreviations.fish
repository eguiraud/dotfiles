abbr -a -- cal 'cal -m'
abbr -a -- cp 'cp -i'
abbr -a -- ga 'git add'
abbr -a -- gap 'git add -p'
abbr -a -- gg 'gdb -q --args'
abbr -a -- g git
abbr -a -- gc 'git c'
abbr -a -- gca 'git c --amend'
abbr -a -- gd 'git d'
abbr -a -- gf 'git fetch --prune'
abbr -a -- gl 'git lao'
abbr -a -- gr 'git rebase -i main'
abbr -a -- gs 'git s'
abbr -a -- gp 'git pull --prune'
abbr -a -- gpp 'git push --set-upstream me'
abbr -a -- gpd 'git push --delete me'
abbr -a -- gpf 'git push --force --set-upstream me'
abbr -a -- h 'helix'
abbr -a -- lr 'ls -tr'
abbr -a -- mv 'mv -i'
abbr -a -- p ipython
abbr -a -- readlink 'readlink -e'
abbr -a -- rm 'rm -i'
abbr -a -- root 'root -l'
abbr -a -- r 'ranger'
abbr -a -- s 'env TERM=xterm-256color ssh'
abbr -a -- su 'su -'
abbr -a -- t 'clear && task'
abbr -a -- ts 'task sync'
abbr -a -- valgr 'valgrind'
abbr -a -- v nvim
abbr -a -- w wiki
abbr -a -- pa paru
abbr -a -- c cargo
abbr -a -- cb cargo build
abbr -a -- ct cargo nextest run
